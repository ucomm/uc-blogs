            <footer>
            <?php 
                
                if (is_active_sidebar('footer_widget_1')) {
                    dynamic_sidebar('footer_widget_1');
                }
                if (is_active_sidebar('footer_widget_2')) {
                    dynamic_sidebar('footer_widget_2');
                }
                
                $args = array(
                    'container' => 'nav',
                    'menu_id' => 'footer-menu',
                    'theme_location' => 'footer-menu'
                );
                wp_nav_menu($args);
                
            ?>
            </footer>
        </div><!-- /wrapper -->

        <?php wp_footer(); ?>

    </body>
</html>