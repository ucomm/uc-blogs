<?php

namespace UCBlogs\Lib;

/**
 * A utility class to bootstrap sites and posts for local development
 */

class Bootstrapper {
  /**
   * A getter to check the current server hostname
   */
  public function get_server_host() {
    return $_SERVER['HTTP_HOST'];
  }
  /**
   * A getter to check if the bootstrapping process has been run
   */
  public function get_uc_blogs_setup() {
    return get_option('uc_blogs_setup');
  }
  /**
   * Bootstrap a group of posts for local development
   *
   * @return void
   */
  public function bootstrap_posts() {
    $posts_seed = UC_BLOGS_DIR . "/utils/posts-seed.json";
    $posts = file_get_contents($posts_seed);
    $posts = json_decode($posts);
    foreach ($posts as $post) {
      $args = array(
        'post_content' => $post->content,
        'post_date' => $post->date,
        'post_title' => $post->title,
        'post_status' => 'publish'
      );
      wp_insert_post($args);
    }
    wp_delete_post(1);
    update_option('uc_blogs_setup', true);
  }
  /**
   * This is the method to call in functions.php
   */
  public function init() {
    if (!get_option('uc_blogs_setup')) {
      add_option('uc_blogs_setup', false);
    }
    $this->bootstrap_posts();
  }
}