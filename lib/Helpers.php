<?php

namespace UCBlogs\Lib;

class Helpers {

    public static function load_scripts() {
        wp_enqueue_style('uc-blogs-styles',UC_BLOGS_URL . '/build/main.css');
        wp_enqueue_style('uconn-banner', UC_BLOGS_URL . '/vendor/uconn/banner/_site/banner.css');

        wp_enqueue_script('uc-blogs-script', UC_BLOGS_URL . '/build/index.js', array('jquery'), false, true);
    }
    public static function register_menus() {
        register_nav_menu('main', __('Main Menu', 'uc-blogs'));
        register_nav_menu('footer', __('Footer Menu', 'uc-blogs'));
    }
    public static function register_widgets() {
        register_sidebar(array(
            'name' => __('Footer Widget 1', 'uc-blogs'),
            'id' => 'footer_widget_1',
            'before_widget' => '<div class="widget footer-widget">',
            'after_widget' => '</div>'
        ));
        register_sidebar(array(
            'name' => __('Footer Widget 2', 'uc-blogs'),
            'id' => 'footer_widget_2',
            'before_widget' => '<div class="widget footer-widget">',
            'after_widget' => '</div>'
        ));
    }
    /**
     * This function will change category urls from /category/{cat-name} to /{cat-name}
     * It is hooked into the category_rewrite_rules filter which is a method of WP_Rewrite
     *
     * @param array $cat_rewrite - an array of the category rewrite rules
     * @return array - the modified $cat_rewrite array
     */
    public static function category_rewrite($cat_rewrite) {
  
        $categories = get_categories(array(
            'hide_empty' => false
        ));

        foreach ( $categories as $category ) {
            $category_nicename = $category->slug;
            if (  $category->parent == $category->cat_ID ) {
                $category->parent = 0;
            } elseif ( 0 != $category->parent ) {
                $category_nicename = get_category_parents(  $category->parent, false, '/', true  ) . $category_nicename;
            }
            $cat_rewrite[ '(' . $category_nicename . ')/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$' ] = 'index.php?category_name=$matches[1]&feed=$matches[2]';
            $cat_rewrite[ '(' . $category_nicename . ')/page/?([0-9]{1,})/?$' ] = 'index.php?category_name=$matches[1]&paged=$matches[2]';
            $cat_rewrite[ '(' . $category_nicename . ')/?$' ] = 'index.php?category_name=$matches[1]';
        }

        // Redirect support from Old Category Base
        $old_category_base = get_option( 'category_base' ) ? get_option( 'category_base' ) : 'category';
        $old_category_base = trim( $old_category_base, '/' );
        $cat_rewrite[ $old_category_base . '/(.*)$' ] = 'index.php?category_redirect=$matches[1]';

        // filter out the category/ part of the url.
        $cat_rewrite = array_filter($cat_rewrite, function($key) {
            if (strpos($key, 'category/') === false ) {
                return $key;
            }
        }, ARRAY_FILTER_USE_KEY);

        return $cat_rewrite;
    }
}