<?php

namespace UCBlogs\Lib;

class ShortcodeHandler {
  static public function show_posts($atts, $content = null) {
    $posts_per_page = get_option('posts_per_page');
    $a = shortcode_atts(array(
      'number' => $posts_per_page
    ), $atts);

    $args = array(
      'numberposts' => (int)$a['number'],
      'post_type' => 'post',
      'post_status' => 'publish'
    );

    $posts = get_posts($args);

    $output = ob_start();
    echo "<ul id='posts-list'>";
    include_once(UC_BLOGS_DIR . '/template-parts/post-details.php');
    echo "</ul>";
    $output = ob_get_clean();
    return $output;
  }
}