<?php

foreach ($posts as $post) {
  $category_array = get_the_category($post->ID);
  $category_name = $category_array[0]->name;
  $category_parent = $category_array[0]->category_parent;
  $excerpt = get_the_excerpt($post->ID);
  $permalink = get_permalink($post->ID);

  if ($category_parent !== 0) {
    $parent = get_category_parents($category_parent);
    $parent = str_replace('/', '', $parent);
    $category_name = $parent . " - " . "$category_name";
  }


?>
  <li class="post-list-item">
    <span class="post-list-cat"><?php echo $category_name; ?></span>
    <h2 class="post-list-title">
      <?php echo $post->post_title; ?>
    </h2>
    <p class="post-list-excerpt"><?php $excerpt; ?></p>
    <a class="post-list-read-more" href="<?php echo $permalink; ?>"
      aria-label="read more about <?php echo $post->post_title; ?>">Read more</a>
  </li>
<?php
}

if ($args['numberposts'] !== -1 && !is_front_page()) {
?>
  <button class="post-list-btn btn-more-posts">More posts</button>
<?php
}