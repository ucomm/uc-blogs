<?php

// Constants
define( 'UC_BLOGS_DIR', get_stylesheet_directory() );
define( 'UC_BLOGS_URL', get_stylesheet_directory_uri() );

// Load
require_once( 'vendor/autoload.php' );
require_once( 'lib/Bootstrapper.php' );
require_once( 'lib/Helpers.php' );
require_once( 'lib/ShortcodeHandler.php' );

// Bootstrap posts for local development
$bootstrapper = new UCBlogs\Lib\Bootstrapper();
$is_setup = $bootstrapper->get_uc_blogs_setup();
$host = $bootstrapper->get_server_host();
if (!$is_setup && $host === 'localhost') {
  add_action('init', array($bootstrapper, 'init'));
}

// Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', array('UCBlogs\Lib\Helpers', 'load_scripts') );

// Register Menus and Widgets
add_action('after_setup_theme', array('UCBlogs\Lib\Helpers', 'register_menus'));
add_action('widgets_init', array('UCBlogs\Lib\Helpers', 'register_widgets'));

// Change category URLs
add_filter('category_rewrite_rules', array('UCBlogs\Lib\Helpers', 'category_rewrite'));

add_shortcode('show_posts', 'UCBlogs\Lib\ShortcodeHandler::show_posts');